Source: hashrat
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Giovani Augusto Ferreira <giovani@debian.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Rules-Requires-Root: binary-targets
Homepage: http://www.cjpaget.co.uk/Code/Hashrat
Vcs-Git: https://salsa.debian.org/pkg-security-team/hashrat.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/hashrat

Package: hashrat
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: hashing tool supporting several hashes and recursivity
 Hashrat is a hash-generation utility that supports the md5, sha1, sha256,
 sha512, whirlpool, jh-244, jh256, jh-384 and jh-512 hash functions, and
 also the HMAC versions of those functions. It can output in 'traditional'
 format (same as md5sum and shasum and the like), or it's own format.
 .
 Hashes can be output in octal, decimal, hexadecimal, uppercase hexadecimal
 or base64.
 .
 Hashrat also supports directory recursion, hashing entire devices,
 generating a hash for an entire directory, operations in remote machines
 and several other features. It has a 'CGI' mode that can be used as a
 web-page to lookup hashes.
 .
 This tool is useful in forensics investigations and network security.
